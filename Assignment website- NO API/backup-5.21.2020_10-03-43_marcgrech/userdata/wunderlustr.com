--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/wunderlustr.com
  - 
    format: "\"%{%s}t %I .\\n%{%s}t %O .\""
    target: /etc/apache2/logs/domlogs/wunderlustr.com-bytes_log
documentroot: /home3/marcgrech/public_html
group: marcgrech
hascgi: 1
homedir: /home3/marcgrech
ip: 50.87.150.249
owner: root
phpopenbasedirprotect: 1
port: 80
scriptalias: 
  - 
    path: /home3/marcgrech/public_html/cgi-bin
    url: /cgi-bin/
serveradmin: webmaster@wunderlustr.com
serveralias: mail.wunderlustr.com www.wunderlustr.com
servername: wunderlustr.com
usecanonicalname: 'Off'
user: marcgrech
