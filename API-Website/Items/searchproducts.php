<?php

if(isset($_POST['search']))
{
    $valueToSearch = $_POST['valueToSearch'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `event` WHERE CONCAT(`Id`, `Name`, `Price`, `Description`) LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else if(isset($_POST['searchbyname']))
{
    $valueToSearch = $_POST['valueToSearchbyName'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `event` WHERE `Name` LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else if(isset($_POST['searchbyprice']))
{
    $valueToSearch = $_POST['valueToSearchbyPrice'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT * FROM `event` WHERE `Price` LIKE '%".$valueToSearch."%'";
    $search_result = filterTable($query);
    
}
 else {
    $query = "SELECT * FROM `event`";
    $search_result = filterTable($query);
}

// function to connect and execute the query
function filterTable($query)
{
    $connect = mysqli_connect("localhost", "root", "", "mydb");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>PHP HTML TABLE DATA SEARCH</title>
        <style>
            table,tr,th,td
            {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        
        <form action="searchproducts.php" method="post">
            <input class="inputtxt" type="text" name="valueToSearch" placeholder="Search from all products...">
            <input class="btn" type="submit" name="search" value="Filter"><br><br>
            
            <input class="inputtxt" type="text" name="valueToSearchbyName" placeholder="Search by name...">
            <input class="btn" type="submit" name="searchbyname" value="Filter"><br><br>
            
            <input  class="inputtxt" type="text" name="valueToSearchbyPrice" placeholder="Search by price...">
            <input  class="btn" type="submit" name="searchbyprice" value="Filter"><br><br>
            <table>
                
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Description</th>
                </tr>

      <!-- populate table from mysql database -->
                <?php while($row = mysqli_fetch_array($search_result)):?>
                <tr>
                    <td><?php echo $row['Id'];?></td>
                    <td><?php echo $row['Name'];?></td>
                    <td><?php echo $row['Price'];?></td>
                    <td><?php echo $row['Description'];?></td>
                </tr>
                <?php endwhile;?>
            </table>
        </form>
        
    </body>
</html>

<style>
    th{
        margin-bottom:20px;
        color:maroon;
        font-size:25px;
        padding-right:30px;
        text-align:left;
    }
    
    td{
        padding:10px;
        color:darkolivegreen;
        font-size:20px;
    }
    
    .btn{
        background-color:saddlebrown;
        color:white;
        border:1px solid saddlebrown;
        border-radius:10px;
        width:150px;
        height:50px;
    }
    
    .inputtxt{
        width:300px;
        height:50px;
        color:black;
        border:2px solid black;
        border-radius:5px;
    }
</style>