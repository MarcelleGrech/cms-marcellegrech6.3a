<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'apiexample' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<pk$O1gQDW[o$0~|wYy)yUiP[0gVq|bp/3R^4R6=!^ze~$<sXk1N$(cWEBYw_>yD' );
define( 'SECURE_AUTH_KEY',  '@ 2Azq#rPZEFuy(H8Ohkkt@e:680{sa ;CKQ~F9xN^YS1H?s b5|dZ1bq8Yo4R8_' );
define( 'LOGGED_IN_KEY',    '^Uue{I2hqqkW2Pchso!5>jiXcm(xk{A&/3#qKQ09wTD?]1G/*,L;|g7As.&0z4aI' );
define( 'NONCE_KEY',        '48`=&hbRz7mYm9/1Bzu;:l$s5^@t {&O`/G_>eDS9-8$Q>$hCi=rwxkE9]Hb#sYx' );
define( 'AUTH_SALT',        '[mUAB?+>%@L3SJq&zD0ri0MNX^[l4O;6F(hq.H.gexIfFOD4G+ESoUpSLQ,|)O%;' );
define( 'SECURE_AUTH_SALT', 'IkzI;[-Te +{Kv7QNe%tQ0Z{bs,IoHU{g5}U-U-1]/G62Wm!8:`Z.}H&(hh@Nj1L' );
define( 'LOGGED_IN_SALT',   'jA6l :Kk-DhdW0bTlp2 kS2|W:!~R^)ORNi:nl-~fzT{$vu8FA6w(/u3&+|p.Gc#' );
define( 'NONCE_SALT',       'b3;kmvbz)]aWZe(N.Oy(3Me+]/)Fw4^&V}D?Bkie1$0v3BZ%b3wz3j;pi2vw[j|m' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
