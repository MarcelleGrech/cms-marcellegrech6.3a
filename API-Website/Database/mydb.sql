-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2020 at 05:59 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `Id` int(100) NOT NULL,
  `Name` text NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`Id`, `Name`, `Price`, `Description`) VALUES
(3, 'european jewels', '1099', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(4, 'mon cher package', '2390', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(5, 'beautiful mallorca', '400', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(6, 'the italian explorer', '2099', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(7, 'magical fjords', '4000', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(8, 'paradise islands', '1290', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(3, 'european jewels', '1099', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(4, 'mon cher package', '2390', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(5, 'beautiful mallorca', '400', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(6, 'the italian explorer', '2099', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(7, 'magical fjords', '4000', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. '),
(8, 'paradise islands', '1290', 'Duis sollicitudin rutrum ante et tempor. Mauris eget porttitor ipsum. Sed id mi et arcu eleifend cursus. Vestibulum ante felis, gravida nec pellentesque ac, sollicitudin tempor nunc. ');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
